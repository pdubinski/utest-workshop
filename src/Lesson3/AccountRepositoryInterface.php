<?php

declare(strict_types=1);

namespace App\Lesson3;

interface AccountRepositoryInterface
{
    public function save(AccountInterface $account);
}
