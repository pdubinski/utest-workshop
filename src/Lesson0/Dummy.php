<?php

declare(strict_types=1);

namespace App\Lesson0;

class Dummy
{
    public function add(int $a, int $b) : int
    {
        return $a + $b;
    }
}
