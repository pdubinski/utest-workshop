<?php

declare(strict_types=1);

namespace Tests\App\Lesson2;

use App\Lesson2\SoldierDamageReactionStrategy;
use App\Lesson2\SoldierInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use App\Lesson1\DamageCalculator;

class SoldierDamageReactionStrategyTest extends TestCase
{

    /**
     * @test
     */
    public function reactAfterGettingDamageWillCallCryAndLeaveThisWorldWhenHealthStatusIsDead()
    {
        $healthStatus = DamageCalculator::HEALTH_STATUS_DEAD;

        //$soldierMock = $this->getMockBuilder(SoldierInterface::class)->disableOriginalConstructor()->getMock();

        /**
         * @var SoldierInterface|\PHPUnit_Framework_MockObject_MockObject $soldierMock
         */
        $soldierMock = $this->createMock(SoldierInterface::class);

        $strategy = new SoldierDamageReactionStrategy();

        $soldierMock->expects($this->once())->method('cry');
        $soldierMock->expects($this->once())->method('leaveThisWorld');

        $strategy->reactAfterGettingDamage($healthStatus, $soldierMock);
    }

}
