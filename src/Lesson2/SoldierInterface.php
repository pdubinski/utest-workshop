<?php

namespace App\Lesson2;

interface SoldierInterface
{
    public function smile();
    public function groan();
    public function shout();
    public function cry();
    public function leaveThisWorld();
    public function shoot();
}
