<?php

declare(strict_types=1);

namespace App\Lesson3;

/**
 * @package App\Lesson3
 */
class MoneyTransfer
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MoneyTransfer constructor
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param AccountInterface $accountFrom
     * @param AccountInterface $accountTo
     * @param int $value
     * @return bool
     */
    public function transferMoney(AccountInterface $accountFrom, AccountInterface $accountTo, int $value) : bool
    {
        try {
            $accountFrom->decreaseBalance($value);
            $accountTo->increaseBalance($value);
            $this->em->flush();

            // send email
            mail($accountTo->getEmail(), 'New incoming transfer', 'You have more money!');

            // call api to send mobile notification
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "api.mobile.bank.com/notify?user=" . $accountTo->getId() . "&value=" . $value);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_exec($ch);
            curl_close($ch);
        } catch (\Exception $e) {
            return false;
        }
    }
}
