<?php

declare(strict_types=1);

namespace App\Lesson2;

use App\Lesson1\DamageCalculator;

class SoldierDamageReactionStrategy
{
    /**
     * When HEALTH_STATUS_HEALTHY - smile and shoot twice
     * When HEALTH_STATUS_MINOR_DAMAGE - groan and shoot
     * When HEALTH_STATUS_SEVERE_DAMAGE - shout and shoot
     * When HEALTH_STATUS_DEAD - cry and die
     * @param string $healthStatus
     * @param SoldierInterface $soldier
     */
    public function reactAfterGettingDamage(string $healthStatus, SoldierInterface $soldier)
    {
        switch ($healthStatus) {
            case DamageCalculator::HEALTH_STATUS_HEALTHY: {
                $soldier->smile();
                $soldier->shoot();
                $soldier->shoot();
                break;
            }
            case DamageCalculator::HEALTH_STATUS_MINOR_DAMAGE:
            {
                $soldier->groan();
                $soldier->shoot();
                break;
            }
            case DamageCalculator::HEALTH_STATUS_SEVERE_DAMAGE:
            {
                $soldier->shout();
                $soldier->shoot();
                break;
            }
            case DamageCalculator::HEALTH_STATUS_DEAD:
            {
                $soldier->cry();
                $soldier->leaveThisWorld();
                break;
            }
        }
    }
}
