<?php

declare(strict_types=1);

namespace App\Lesson3;

interface ConnectionInterface
{
    public function commit();
    public function rollback();
    public function beginTransaction();
}