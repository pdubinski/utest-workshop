<?php

declare(strict_types=1);

namespace App\Lesson1;

class DamageCalculator
{
    const HEALTH_STATUS_HEALTHY = 'healthy';
    const HEALTH_STATUS_MINOR_DAMAGE = 'minor_damage';
    const HEALTH_STATUS_SEVERE_DAMAGE = 'severe_damage';
    const HEALTH_STATUS_DEAD = 'dead';

    /**
     * @param int $currentEnergyLevel
     * @param int $damageReceived
     * @return string
     */
    public function getHealthStatusAfterReceivingDamage(int $currentEnergyLevel, int $damageReceived) : string
    {
        if ($currentEnergyLevel <= 0) {
            return static::HEALTH_STATUS_DEAD;
        }

        if ($damageReceived < 0) {
            throw new InvalidDamageException();
        }

        $energyAfterDamage = $currentEnergyLevel - $damageReceived;
        if ($energyAfterDamage >= 80) {
            $healthStatus = static::HEALTH_STATUS_HEALTHY;
        } else if ($energyAfterDamage >= 60 && $energyAfterDamage < 80) {
            $healthStatus = static::HEALTH_STATUS_MINOR_DAMAGE;
        } else if ($energyAfterDamage >= 1 && $energyAfterDamage < 60) {
            $healthStatus = static::HEALTH_STATUS_SEVERE_DAMAGE;
        } else {
            $healthStatus = static::HEALTH_STATUS_DEAD;
        }
        return $healthStatus;
    }
}
