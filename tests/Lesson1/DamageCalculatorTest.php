<?php

declare(strict_types=1);

namespace Tests\App\Lesson1;

use App\Lesson1\DamageCalculator;
use App\Lesson1\InvalidDamageException;
use PHPUnit\Framework\TestCase;

class DamageCalculatorTest extends TestCase
{

//    /**
//     * @test
//     */
//    public function getHealthStatusAfterReceivingDamageWillReturnStatusDeadWhenDamageIsLargerThanCurrentHealth()
//    {
//        $currentHealth = 50;
//        $damageReceived = 100;
//
//        $damageCalculator = new DamageCalculator();
//        $status = $damageCalculator->getHealthStatusAfterReceivingDamage($currentHealth, $damageReceived);
//
//        $this->assertEquals(DamageCalculator::HEALTH_STATUS_DEAD, $status);
//    }


    /**
     * @test
     * @dataProvider healthDamageStatusProvider
     */
    public function getHealthStatusAfterReceivingDamage_WillReturnCorrectHealthStatus(int $startHp, int $hitPoint, string $expectedStatus)
    {
        $damageCalculator = new DamageCalculator();
        $returnedStatus = $damageCalculator->getHealthStatusAfterReceivingDamage($startHp, $hitPoint);
        $this->assertEquals($expectedStatus, $returnedStatus);
    }

    public function healthDamageStatusProvider()
    {
        return array(
            'test Roki' => [100, 20, DamageCalculator::HEALTH_STATUS_HEALTHY],
            [100, 40, DamageCalculator::HEALTH_STATUS_MINOR_DAMAGE],
            [100, 50, DamageCalculator::HEALTH_STATUS_SEVERE_DAMAGE],
            [100, 100, DamageCalculator::HEALTH_STATUS_DEAD],
            [-50, 100, DamageCalculator::HEALTH_STATUS_DEAD],
            [-50, -100, DamageCalculator::HEALTH_STATUS_DEAD],
            [-100, 100, DamageCalculator::HEALTH_STATUS_DEAD],
            [-100, 100, DamageCalculator::HEALTH_STATUS_DEAD],
        );
    }

    /**
     * @test
     *
     */
    public function getHealthStatusAfterReceivingDamageWillThrowExceptionWhenDamageReceivedIsNegative()
    {
        $this->expectException(InvalidDamageException::class);
        $currentHealth = 50;
        $damageReceived = -100;
        $damageCalculator = new DamageCalculator();
        $damageCalculator->getHealthStatusAfterReceivingDamage($currentHealth, $damageReceived);
        $damageCalculator->getHealthStatusAfterReceivingDamage($currentHealth, $damageReceived);
    }

}
