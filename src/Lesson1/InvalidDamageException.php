<?php
/**
 * Created by PhpStorm.
 * User: paweldubinski
 * Date: 22.05.2018
 * Time: 13:51
 */

namespace App\Lesson1;


class InvalidDamageException extends \Exception
{

    /**
     * InvalidDamageException constructor.
     */
    public function __construct()
    {
    }
}