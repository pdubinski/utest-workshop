<?php

declare(strict_types=1);

namespace Tests\App\Lesson0;

use App\Lesson0\Dummy;
use PHPUnit\Framework\TestCase;

class DummyTest extends TestCase
{
    /**
     * @test
     */
    public function testAddWillReturnSumOfTwoNumbers()
    {
        $dummy = new Dummy();
        $this->assertEquals(4, $dummy->add(2, 2));
    }
}
