<?php

declare(strict_types=1);

namespace App\Lesson3;

interface AccountInterface
{
    public function decreaseBalance($value);
    public function increaseBalance($value);
    public function getBalance($value) : int;
    public function getEmail() : string;
    public function getId() : string;
}