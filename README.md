## Installation
composer install

## Refreshing autoload after adding new files
composer dump-autoload -o

## Running tests:
vendor/bin/phpunit -c phpunit.xml --coverage-html coverage