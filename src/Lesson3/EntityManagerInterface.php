<?php

namespace App\Lesson3;

interface EntityManagerInterface
{
    public function getRepository() : RepositoryInterface;
    public function flush();
    public function persist();
}