<?php

declare(strict_types=1);

namespace App\Lesson3;

/**
 * @package App\Lesson3
 */
class MoneyTransfer2
{
    /**
     * @var AccountRepositoryInterface
     */
    private $accountRepository;

    /**
     * @var ConnectionInterface
     */
    private $connection;

    /**
     * MoneyTransfer2 constructor.
     * @param AccountRepositoryInterface $accountRepository
     * @param ConnectionInterface $connection
     */
    public function __construct(AccountRepositoryInterface $accountRepository, ConnectionInterface $connection)
    {
        $this->accountRepository = $accountRepository;
        $this->connection = $connection;
    }

    /**
     * @param AccountInterface $accountFrom
     * @param AccountInterface $accountTo
     * @param int $value
     * @return bool
     */
    public function transferMoney(AccountInterface $accountFrom, AccountInterface $accountTo, int $value) : bool
    {
        try {
            $this->connection->beginTransaction();
            $accountFrom->decreaseBalance($value);
            $accountTo->increaseBalance($value);
            $this->connection->commit();
        } catch (\Exception $e) {
            $this->connection->rollback();
            return false;
        }
    }
}
